plugins {
    id("com.android.application")
    kotlin("android")
    id("kotlin-android")
    id("androidx.navigation.safeargs.kotlin")
}

dependencies {
    implementation(project(":shared"))
    implementation("com.google.android.material:material:1.3.0")
    implementation("androidx.appcompat:appcompat:1.2.0")
    implementation("androidx.constraintlayout:constraintlayout:2.0.4")
    implementation("org.jetbrains.kotlin:kotlin-stdlib:${rootProject.extra["kotlin_version"]}")
    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.2.0")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0")
    implementation("androidx.fragment:fragment:1.2.5")
    implementation("androidx.fragment:fragment-ktx:1.2.5")

    // Java language implementation
    implementation ("androidx.navigation:navigation-fragment:2.3.3")
    implementation ("androidx.navigation:navigation-ui:2.3.3")

    // Kotlin
    implementation ("androidx.navigation:navigation-fragment-ktx:2.3.3")
    implementation ("androidx.navigation:navigation-ui-ktx:2.3.3")

    // Feature module Support
    implementation ("androidx.navigation:navigation-dynamic-features-fragment:2.3.3")
    implementation("androidx.recyclerview:recyclerview:1.1.0")
}

android {
    compileSdkVersion(29)
    defaultConfig {
        applicationId = "com.example.showcase.ShowcaseDroid"
        minSdkVersion(24)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "1.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
    buildFeatures {
        dataBinding = true
    }
}