package com.adadapted.showcase.showcaseDroid;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.adadapted.showcase.showcaseDroid.databinding.ActivityMainBindingImpl;
import com.adadapted.showcase.showcaseDroid.databinding.AdListFragmentBindingImpl;
import com.adadapted.showcase.showcaseDroid.databinding.CampaignFragmentBindingImpl;
import com.adadapted.showcase.showcaseDroid.databinding.ShoppingListFragmentBindingImpl;
import com.adadapted.showcase.showcaseDroid.databinding.SignInFragmentBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYMAIN = 1;

  private static final int LAYOUT_ADLISTFRAGMENT = 2;

  private static final int LAYOUT_CAMPAIGNFRAGMENT = 3;

  private static final int LAYOUT_SHOPPINGLISTFRAGMENT = 4;

  private static final int LAYOUT_SIGNINFRAGMENT = 5;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(5);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.adadapted.showcase.showcaseDroid.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.adadapted.showcase.showcaseDroid.R.layout.ad_list_fragment, LAYOUT_ADLISTFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.adadapted.showcase.showcaseDroid.R.layout.campaign_fragment, LAYOUT_CAMPAIGNFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.adadapted.showcase.showcaseDroid.R.layout.shopping_list_fragment, LAYOUT_SHOPPINGLISTFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.adadapted.showcase.showcaseDroid.R.layout.sign_in_fragment, LAYOUT_SIGNINFRAGMENT);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYMAIN: {
          if ("layout/activity_main_0".equals(tag)) {
            return new ActivityMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
        }
        case  LAYOUT_ADLISTFRAGMENT: {
          if ("layout/ad_list_fragment_0".equals(tag)) {
            return new AdListFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for ad_list_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_CAMPAIGNFRAGMENT: {
          if ("layout/campaign_fragment_0".equals(tag)) {
            return new CampaignFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for campaign_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_SHOPPINGLISTFRAGMENT: {
          if ("layout/shopping_list_fragment_0".equals(tag)) {
            return new ShoppingListFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for shopping_list_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_SIGNINFRAGMENT: {
          if ("layout/sign_in_fragment_0".equals(tag)) {
            return new SignInFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for sign_in_fragment is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(1);

    static {
      sKeys.put(0, "_all");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(5);

    static {
      sKeys.put("layout/activity_main_0", com.adadapted.showcase.showcaseDroid.R.layout.activity_main);
      sKeys.put("layout/ad_list_fragment_0", com.adadapted.showcase.showcaseDroid.R.layout.ad_list_fragment);
      sKeys.put("layout/campaign_fragment_0", com.adadapted.showcase.showcaseDroid.R.layout.campaign_fragment);
      sKeys.put("layout/shopping_list_fragment_0", com.adadapted.showcase.showcaseDroid.R.layout.shopping_list_fragment);
      sKeys.put("layout/sign_in_fragment_0", com.adadapted.showcase.showcaseDroid.R.layout.sign_in_fragment);
    }
  }
}
