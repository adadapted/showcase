package com.adadapted.showcase.showcaseDroid

import android.os.Bundle
import androidx.navigation.NavDirections
import kotlin.Int
import kotlin.String

public class AdListFragmentDirections private constructor() {
  private data class ActionAdListFragmentToDisplayCaseFragment(
    public val groceryArg: String
  ) : NavDirections {
    public override fun getActionId(): Int = R.id.action_adListFragment_to_displayCaseFragment

    public override fun getArguments(): Bundle {
      val result = Bundle()
      result.putString("groceryArg", this.groceryArg)
      return result
    }
  }

  public companion object {
    public fun actionAdListFragmentToDisplayCaseFragment(groceryArg: String): NavDirections =
        ActionAdListFragmentToDisplayCaseFragment(groceryArg)
  }
}
