package com.adadapted.showcase.showcaseDroid

import android.os.Bundle
import androidx.navigation.NavArgs
import java.lang.IllegalArgumentException
import kotlin.String
import kotlin.jvm.JvmStatic

public data class ShoppingListFragmentArgs(
  public val groceryArg: String
) : NavArgs {
  public fun toBundle(): Bundle {
    val result = Bundle()
    result.putString("groceryArg", this.groceryArg)
    return result
  }

  public companion object {
    @JvmStatic
    public fun fromBundle(bundle: Bundle): ShoppingListFragmentArgs {
      bundle.setClassLoader(ShoppingListFragmentArgs::class.java.classLoader)
      val __groceryArg : String?
      if (bundle.containsKey("groceryArg")) {
        __groceryArg = bundle.getString("groceryArg")
        if (__groceryArg == null) {
          throw IllegalArgumentException("Argument \"groceryArg\" is marked as non-null but was passed a null value.")
        }
      } else {
        throw IllegalArgumentException("Required argument \"groceryArg\" is missing and does not have an android:defaultValue")
      }
      return ShoppingListFragmentArgs(__groceryArg)
    }
  }
}
