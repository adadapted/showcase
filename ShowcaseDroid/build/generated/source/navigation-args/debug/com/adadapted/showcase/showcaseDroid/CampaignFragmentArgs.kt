package com.adadapted.showcase.showcaseDroid

import android.os.Bundle
import androidx.navigation.NavArgs
import java.lang.IllegalArgumentException
import kotlin.String
import kotlin.jvm.JvmStatic

public data class CampaignFragmentArgs(
  public val testArg: String
) : NavArgs {
  public fun toBundle(): Bundle {
    val result = Bundle()
    result.putString("testArg", this.testArg)
    return result
  }

  public companion object {
    @JvmStatic
    public fun fromBundle(bundle: Bundle): CampaignFragmentArgs {
      bundle.setClassLoader(CampaignFragmentArgs::class.java.classLoader)
      val __testArg : String?
      if (bundle.containsKey("testArg")) {
        __testArg = bundle.getString("testArg")
        if (__testArg == null) {
          throw IllegalArgumentException("Argument \"testArg\" is marked as non-null but was passed a null value.")
        }
      } else {
        throw IllegalArgumentException("Required argument \"testArg\" is missing and does not have an android:defaultValue")
      }
      return CampaignFragmentArgs(__testArg)
    }
  }
}
