package com.adadapted.showcase.showcaseDroid

import android.os.Bundle
import androidx.navigation.NavArgs
import java.lang.IllegalArgumentException
import kotlin.String
import kotlin.jvm.JvmStatic

public data class AdListFragmentArgs(
  public val adListArg: String
) : NavArgs {
  public fun toBundle(): Bundle {
    val result = Bundle()
    result.putString("adListArg", this.adListArg)
    return result
  }

  public companion object {
    @JvmStatic
    public fun fromBundle(bundle: Bundle): AdListFragmentArgs {
      bundle.setClassLoader(AdListFragmentArgs::class.java.classLoader)
      val __adListArg : String?
      if (bundle.containsKey("adListArg")) {
        __adListArg = bundle.getString("adListArg")
        if (__adListArg == null) {
          throw IllegalArgumentException("Argument \"adListArg\" is marked as non-null but was passed a null value.")
        }
      } else {
        throw IllegalArgumentException("Required argument \"adListArg\" is missing and does not have an android:defaultValue")
      }
      return AdListFragmentArgs(__adListArg)
    }
  }
}
