package com.adadapted.showcase.showcaseDroid

import android.os.Bundle
import androidx.navigation.NavDirections
import kotlin.Int
import kotlin.String

public class SignInFragmentDirections private constructor() {
  private data class ActionSignInFragmentToCampaignFragment(
    public val testArg: String
  ) : NavDirections {
    public override fun getActionId(): Int = R.id.action_signInFragment_to_campaignFragment

    public override fun getArguments(): Bundle {
      val result = Bundle()
      result.putString("testArg", this.testArg)
      return result
    }
  }

  public companion object {
    public fun actionSignInFragmentToCampaignFragment(testArg: String): NavDirections =
        ActionSignInFragmentToCampaignFragment(testArg)
  }
}
