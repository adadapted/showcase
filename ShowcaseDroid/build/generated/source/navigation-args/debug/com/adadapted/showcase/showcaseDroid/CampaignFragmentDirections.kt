package com.adadapted.showcase.showcaseDroid

import android.os.Bundle
import androidx.navigation.NavDirections
import kotlin.Int
import kotlin.String

public class CampaignFragmentDirections private constructor() {
  private data class ActionCampaignFragmentToAdListFragment(
    public val adListArg: String
  ) : NavDirections {
    public override fun getActionId(): Int = R.id.action_campaignFragment_to_adListFragment

    public override fun getArguments(): Bundle {
      val result = Bundle()
      result.putString("adListArg", this.adListArg)
      return result
    }
  }

  public companion object {
    public fun actionCampaignFragmentToAdListFragment(adListArg: String): NavDirections =
        ActionCampaignFragmentToAdListFragment(adListArg)
  }
}
