package com.adadapted.showcase.showcaseDroid

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.adadapted.showcase.showcaseDroid.data.ItemFragmentType
import com.adadapted.showcase.showcaseDroid.databinding.CampaignFragmentBinding
import com.adadapted.showcase.showcaseDroid.viewmodel.CampaignViewModel

class CampaignFragment : Fragment() {

    companion object {
        fun newInstance() = CampaignFragment()
    }

    private lateinit var viewModel: CampaignViewModel
    private lateinit var binding: CampaignFragmentBinding
    val args: CampaignFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.campaign_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(CampaignViewModel::class.java)
        var test = args.testArg //TODO replace with User object (env and campaigns)

        val ft: FragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
        ft.replace(R.id.campaign_list_frag_container, ItemFragment.newInstance(ItemFragmentType.Campaign))
        ft.commit()
    }

}