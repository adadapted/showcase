package com.adadapted.showcase.showcaseDroid

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.adadapted.showcase.showcaseDroid.databinding.SignInFragmentBinding
import com.adadapted.showcase.showcaseDroid.viewmodel.SignInViewModel

class SignInFragment : Fragment() {

    companion object {
        fun newInstance() = SignInFragment()
    }

    private lateinit var viewModel: SignInViewModel
    private lateinit var binding: SignInFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.sign_in_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SignInViewModel::class.java)

        binding.emailText.doOnTextChanged { _, _, _, _ -> binding.outlinedEmailTextField.error = null }
        binding.passwordText.doOnTextChanged { _, _, _, _ -> binding.outlinedPasswordTextField.error = null }

        binding.signInButton.setOnClickListener {
            if (validateInput()) {
                val navController = this.findNavController()
                val action = SignInFragmentDirections.actionSignInFragmentToCampaignFragment("testValue")
                navController.navigate(action)
                //login
                //prevent back logic from logging out / store login info for X amount
            }
        }
    }

    private fun validateInput(): Boolean {
        var isValid = true
        if (binding.outlinedEmailTextField.editText?.text.isNullOrEmpty()) {
            binding.outlinedEmailTextField.error = "Email cannot be empty."
            isValid = false
        }
        if (binding.outlinedPasswordTextField.editText?.text.isNullOrEmpty()) {
            binding.outlinedPasswordTextField.error = "Password cannot be empty."
            isValid = false
        }
        return isValid
    }

}