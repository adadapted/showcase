package com.adadapted.showcase.showcaseDroid

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adadapted.showcase.showcaseDroid.data.ItemFragmentType
import com.adadapted.showcase.showcaseDroid.dummy.DummyContent

class ItemFragment : Fragment() {

    private var itemFragmentType: ItemFragmentType = ItemFragmentType.Dummy

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            itemFragmentType = ItemFragmentType.valueOf(it.getString(ITEM_FRAGMENT_TYPE).toString())
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_item_list, container, false)
        
        if (view is RecyclerView) {
            with(view) {
                layoutManager = LinearLayoutManager(context)
                adapter = ItemRecyclerViewAdapter(DummyContent.ITEMS, itemFragmentType)
            }
        }
        return view
    }

    companion object {
        const val ITEM_FRAGMENT_TYPE = "item-frag-type"

        @JvmStatic
        fun newInstance(itemType: ItemFragmentType) =
            ItemFragment().apply {
                arguments = Bundle().apply {
                    putString(ITEM_FRAGMENT_TYPE, itemType.name)
                }
            }
    }
}