package com.adadapted.showcase.showcaseDroid

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import com.adadapted.showcase.showcaseDroid.data.ItemFragmentType
import com.adadapted.showcase.showcaseDroid.dummy.DummyContent.DummyItem

/**
 * TODO: Replace the implementation with code for graphql data type.
 */
class ItemRecyclerViewAdapter(
    private val values: List<DummyItem>,
    private val itemFragmentType: ItemFragmentType
) : RecyclerView.Adapter<ItemRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.contentView.text = item.content
        holder.contentView.setOnClickListener {
            if (itemFragmentType == ItemFragmentType.Campaign) {
                val navController = holder.contentView.findNavController()
                val action = CampaignFragmentDirections.actionCampaignFragmentToAdListFragment("testCampaignValue")
                navController.navigate(action)
            }
            if (itemFragmentType == ItemFragmentType.Ad) {
                val navController = holder.contentView.findNavController()
                val action = AdListFragmentDirections.actionAdListFragmentToDisplayCaseFragment("testAdValue")
                navController.navigate(action)
            }
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val contentView: TextView = view.findViewById(R.id.content)

        override fun toString(): String {
            return super.toString() + " '" + contentView.text + "'"
        }
    }
}