package com.adadapted.showcase.showcaseDroid

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.fragment.navArgs
import com.adadapted.showcase.showcaseDroid.data.ItemFragmentType
import com.adadapted.showcase.showcaseDroid.databinding.ShoppingListFragmentBinding
import com.adadapted.showcase.showcaseDroid.viewmodel.ShoppingListViewModel

class ShoppingListFragment : Fragment() {

    companion object {
        fun newInstance() = ShoppingListFragment()
    }

    private lateinit var viewModel: ShoppingListViewModel
    private lateinit var binding: ShoppingListFragmentBinding
    val args: ShoppingListFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.shopping_list_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ShoppingListViewModel::class.java)

        binding.adWebview.loadUrl("https://sandbox.adadapted.com/a/NWY0NTM2YZDMMDQ0;101930;45452")

        var test = args.groceryArg //TODO replace with ad zone objects?

        val ft: FragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
        ft.replace(R.id.shopping_list_frag_container, ItemFragment.newInstance(ItemFragmentType.Dummy)) //pass in dummy item objects
        ft.commit()
    }

}