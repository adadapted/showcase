package com.adadapted.showcase.showcaseDroid.data

enum class ItemFragmentType {
    Campaign,
    Ad,
    Dummy
}