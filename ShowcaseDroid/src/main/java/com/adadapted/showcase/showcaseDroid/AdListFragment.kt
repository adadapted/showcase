package com.adadapted.showcase.showcaseDroid

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.fragment.navArgs
import com.adadapted.showcase.showcaseDroid.data.ItemFragmentType
import com.adadapted.showcase.showcaseDroid.databinding.AdListFragmentBinding
import com.adadapted.showcase.showcaseDroid.viewmodel.AdListViewModel

class AdListFragment : Fragment() {

    companion object {
        fun newInstance() = AdListFragment()
    }

    private lateinit var viewModel: AdListViewModel
    private lateinit var binding: AdListFragmentBinding
    val args: AdListFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.ad_list_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(AdListViewModel::class.java)

        var test = args.adListArg //TODO replace with Ad objects?

        val ft: FragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
        ft.replace(R.id.ad_list_frag_container, ItemFragment.newInstance(ItemFragmentType.Ad)) //pass in ad objects
        ft.commit()
    }

}