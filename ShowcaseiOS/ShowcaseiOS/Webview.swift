//
//  Webview.swift
//  ShowcaseiOS
//
//  Created by Brett Clifton on 2/8/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import WebKit
import Foundation

struct Webview: UIViewRepresentable {
    
    var url: String
    
    func makeUIView(context: Context) -> WKWebView {
        guard let url = URL(string: self.url) else {
            return WKWebView()
        }
        
        let request = URLRequest(url: url)
        let wkWebview = WKWebView()
        wkWebview.load(request)
        return wkWebview
    }
    
    func updateUIView(_ uiView: WKWebView, context: UIViewRepresentableContext<Webview>) {
        
    }
}
