//
//  ShoppingListView.swift
//  ShowcaseiOS
//
//  Created by Brett Clifton on 2/8/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct ShoppingListView: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 0, content: {
            TitleText(titleText: "Shopping List")
            Webview(url: "https://sandbox.adadapted.com/a/NWY0NTM2YZDMMDQ0;101930;45452").frame(minWidth: 0, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, minHeight: 60, idealHeight: 60, maxHeight: 60, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).padding(.top, 24).padding(.bottom, -24)
            List {
                ContentRow(contentName: "Barilla Pasta")
                ContentRow(contentName: "3M Products")
                ContentRow(contentName: "Folgers Coffee")
                ContentRow(contentName: "Tyson Chicken Nuggets")
                ContentRow(contentName: "Ripple Milk")
            }.padding(.leading, -16).padding(.top, 24)
            
        }).padding(24)
    }
}

struct ShoppingListView_Previews: PreviewProvider {
    static var previews: some View {
        ShoppingListView()
    }
}
