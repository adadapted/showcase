//
//  AdsView.swift
//  ShowcaseiOS
//
//  Created by Brett Clifton on 2/8/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct AdsView: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 0, content: {
            TitleText(titleText: "Available Ads")
            List {
                NavigationLink(destination: ShoppingListView()) { ContentRow(contentName: "Barilla Pasta") }
                ContentRow(contentName: "3M Products")
                ContentRow(contentName: "Folgers Coffee")
                ContentRow(contentName: "Tyson Chicken Nuggets")
                ContentRow(contentName: "Ripple Milk")
            }.padding(.leading, -16).padding(.top, 24)
        }).padding(24)
    }
}

struct AdsView_Previews: PreviewProvider {
    static var previews: some View {
        AdsView()
    }
}
