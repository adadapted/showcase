//
//  CampaignView.swift
//  ShowcaseiOS
//
//  Created by Brett Clifton on 2/3/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct CampaignView: View {
    @State var campaignListData: DummyContent = DummyContent()
    @State var environment: String = "Dev"
    
    init(){
        UITableView.appearance().backgroundColor = .clear
    }
    var body: some View {
        VStack(alignment: .leading, spacing: 0, content: {
            //TODO Only using dev environment to begin with
//            TitleText(titleText: "Environment")
//            Picker("Environment", selection: $environment) {
//                Text("Development").tag("Dev")
//                Text("Production").tag("Prod")
//            }.pickerStyle(SegmentedPickerStyle()).padding(.top, 24).padding(.bottom, 36)
            //Spacer()
            //Spacer()
            TitleText(titleText: "Campaigns")
            List {
                NavigationLink(destination: AdsView()) {ContentRow(contentName: $campaignListData.name.wrappedValue)}
                ContentRow(contentName: $campaignListData.name.wrappedValue)
                ContentRow(contentName: $campaignListData.name.wrappedValue)
                ContentRow(contentName: $campaignListData.name.wrappedValue)
                ContentRow(contentName: $campaignListData.name.wrappedValue)
            }.padding(.leading, -16).padding(.top, 24)
        }).padding(24)
    }
}

struct CampaignView_Previews: PreviewProvider {
    static var previews: some View {
        CampaignView().environmentObject(DummyContent())
    }
}

struct TitleText: View {
    var titleText: String
    var body: some View {
        Text(titleText)
            .font(.largeTitle)
            .fontWeight(.semibold)
            .foregroundColor(Color("AABlue"))
    }
}
