//
//  SignInView.swift
//  ShowcaseiOS
//
//  Created by Brett Clifton on 2/2/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared

//from shared kmm
func greet() -> String {
    return Greeting().sharedGreeting()
}

struct SignInView: View {
    @State var username: String = ""
    @State var password: String = ""
    @State var authenticationDidFail: Bool = false
    @State var authenticationDidSucceed: Bool = false
    @State var isValid: Bool = true
    
    var body: some View {
        NavigationView {
            VStack {
                AppImage()
                AppText()
                UsernameTextView(username: $username)
                PasswordTextView(password: $password)
                Spacer()
                Spacer()
                
                if authenticationDidFail {
                    Text("Information not correct. Try again.")
                        .foregroundColor(.red)
                }
                if !isValid {
                    Text("Must enter a username and password.")
                        .foregroundColor(.red)
                }
                
                NavigationLink(destination: CampaignView(), isActive: $authenticationDidSucceed) { EmptyView() }
                
                Button(action: {
                    if (username.isEmpty || password.isEmpty) {
                        isValid = false
                    } else {
                        isValid = true
                    }
                    print("Button tapped")
                    
                    if (isValid) { //and signin successful
                        authenticationDidSucceed = true //set from signin call
                    }
                }) {
                    SignInButtonContent()
                }.navigationBarTitle("Back", displayMode: .inline)
            }
            .padding(4)
        }
    }
}

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
    }
}

struct AppText: View {
    var body: some View {
        Text("Showcase")
            .font(.largeTitle)
            .fontWeight(.semibold)
            .padding(.bottom, 20)
            .padding(.top, -36)
            .foregroundColor(Color("AAOrange"))
    }
}

struct AppImage: View {
    var body: some View {
        Image("aa-logo")
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: 350, height: 150)
            .clipped()
    }
}

struct SignInButtonContent: View {
    var body: some View {
        Text("Sign In")
            .font(.headline)
            .foregroundColor(.white)
            .padding()
            .frame(minWidth: 0, maxWidth: .infinity)
            .frame(height: 60, alignment: .center)
            .background(Color("AAOrange"))
            .cornerRadius(15.0)
            .padding(16)
    }
}

struct UsernameTextView: View {
    @Binding var username: String
    var body: some View {
        TextField("Username", text: $username)
            .padding()
            .background(Color("LightGray"))
            .cornerRadius(5)
            .padding(20)
            .shadow(radius: 1)
            .autocapitalization(.none)
    }
}

struct PasswordTextView: View {
    @Binding var password: String
    var body: some View {
        SecureField("Password", text: $password)
            .padding()
            .background(Color("LightGray"))
            .cornerRadius(5)
            .padding(20)
            .padding(.top, -16)
            .shadow(radius: 1)
    }
}
