//
//  DummyContent.swift
//  ShowcaseiOS
//
//  Created by Brett Clifton on 2/3/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation

final class DummyContent: ObservableObject {
    @Published var name = "DummyContent"
}
