//
//  ContentRow.swift
//  ShowcaseiOS
//
//  Created by Brett Clifton on 2/3/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct ContentRow: View {
    @State var contentName = ""
    var body: some View {
        Text(contentName)
    }
}

struct ContentRow_Previews: PreviewProvider {
    static var previews: some View {
        ContentRow()
    }
}
