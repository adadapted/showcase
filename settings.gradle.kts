pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
    
}
rootProject.name = "Showcase"


include(":showcaseDroid")
include(":shared")

