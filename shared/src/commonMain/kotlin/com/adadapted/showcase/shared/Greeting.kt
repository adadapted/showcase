package com.adadapted.showcase.shared

class Greeting {
    fun greeting(): String {
        return "Hello, ${Platform().platform}!"
    }

    fun sharedGreeting(): String {
        return "Shared Greeting"
    }
}
